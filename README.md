## Trabajo Practico N3 Actualizacion Tecnologica - ISTEA


Objetivo: Levantar una aplicacion cualquiera en k8 con volumenes persistentes utilizando microk8s.

Se trabajo en CentOS7 utilizando microk8s via snap.

Antes del depliegue verificar que micrk8s tenga los servicios de DNS y Storage activados.

Verificamos disponibilidad de espacio previa creacion del directorio donde se asignara el volumen
```
df -h /opt/
```

Creamos el directorio
```
mkdir -p /opt/storage/grafana
```

Creamos el volumen
```
microk8s kubectl apply -f pv-grafana.yaml
```

Reclamamos el espacio para nuestro servicio
```
microk8s kubectl apply -f pvc-grafana.yaml
```

Desplegamos la aplicacion
```
microk8s kubectl apply -f deployment-grafana.yaml
```

Creamos el servicio que nos permitiara acceder via web a nuestra aplicacion utilizando servicio tipo NodePort (por default relaciona el puerto de la aplicacion con un puerto del host en rango 30000-32000)
```
service-grafana.yaml
```

Verificamos el puerto asignado en el servicio 
```
microk8s kubectl get svc
```

Finalmente utilizamos dicho puerto con la ip del host para ingresar a la aplicacion.

---

<br />

Notas:

En el primer desplieuge, aun apoyandome en la documentacion oficial, no lograba que la aplicacion levante cuando utilizaba volumenes persistentes. Levantaba sin volumenes, levantaba sin utilizar el campo "classStorage" pero esto generaba voluemens efimeros.
Finalmente, observando logs se verificaba un problema de permisos y remitia a un bug.
```
microk8s kubectl logs grafana-7f79785645-pjbnb
```

El Bug:
```
You may have issues with file permissions, more information here: https://grafana.com/docs/grafana/latest/installation/docker/#migrate-to-v51-or-later
mkdir: can't create directory "/var/lib/grafana/plugin": Permission denied 
```

Para solucionarlo se agrega en el deployment un campo que indica con que usuario inicializar la aplicacion

```
securityContext:
  runAsUser: 0
```
Se podria mejorar el workaround estudiando los campos de "securityContext" y siendo mas especifico en la asignacion del usuario o incluso modificar los permisos en la imagen.

<br />

